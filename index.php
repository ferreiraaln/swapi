<?php
header('Content-Type: application/json');

require_once(__DIR__ . '\app\\autoload.php');
require_once(__DIR__ . '\app\\config.php');

$autoload = new Autoload();

#init
$container = new StarshipAction(new StarshipService(new Starship()));

$result =  $container->index();
echo json_encode($result);
