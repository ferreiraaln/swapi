<?php

class StarshipAction
{

    protected $starshipService;

    function __construct(StarshipService $starshipService)
    {
        $this->starshipService = $starshipService;
    }
    public function index()
    {
        return $this->starshipService->calcMGLT();
    }
}
