<?php

class StarshipService
{
    protected $starship;

    protected $types = array(
        "day" => 24,
        "days" => 24,
        "week" => 168,
        "weeks" => 168,
        "month" => 730.001,
        "months" => 730.001,
        "year" => 8760,
        "years" => 8760
    );

    function __construct(Starship $starship)
    {
        $this->starship = $starship;
    }

    public function calcMGLT()
    {
        $result = [];
        $data = $this->starship->get();
        $mglt = false;

        if (!isset($_GET['mglt'])) {
            return $data;
        } else {
            $mglt = $_GET['mglt'];
        }

        if (!$data) {
            return [];
        }

        foreach ($data->results as $value) {
            $calc = $this->formuleMGLT($mglt, $value->MGLT, $value->consumables);
            array_push($result, $value->name . ':' . (int)$calc);
        }
        return $result;
    }

    /**
     * formule MGLT
     */
    public function formuleMGLT($qtdMglt, $mglt, $consumables)
    {
        return ($qtdMglt / (int) $mglt) / $this->parseHours($consumables);
    }

    /**
     * Convert to hours
     */
    public function parseHours($consumables)
    {
        try {
            $stringArray = explode(" ", $consumables);
            $units = (int)($stringArray[0]);
            $type = $stringArray[1];

            if ($this->types[$type]) {
                return $units * $this->types[$type];
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            return 0;
        }
    }
}
